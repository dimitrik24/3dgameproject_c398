﻿using UnityEngine;
using System.Collections;

//Reference tutorial: https://www.youtube.com/watch?v=mbm9lPB5GPw

public class charControl : MonoBehaviour {

	public float moveSpeed = 12.0F;
	private float mouseSensitivity = 5.0F;

	private float verticalRotation = 0.0F;
	private float tiltLimit = 60.0F;

	private float verticalVelocity = 0.0F;
	public float jumpSpeed = 9.0F;

	private bool touching = false;

	CharacterController characterController;

	// Use this for initialization
	void Start () {

		characterController = GetComponent<CharacterController> ();

		Cursor.visible = false;

	}
	
	// Update is called once per frame
	void Update () {

		//////////Rotation//////////
		//Left/Right rotation
		float rotPan = Input.GetAxis ("Mouse X") * mouseSensitivity;
		//Apply rotation
		transform.Rotate (0, rotPan, 0);

		//Up/Down Rotation
		//Subtracting the mouse value to the base value (verticalRotation) allows to get constant a range of values
		verticalRotation -= Input.GetAxis ("Mouse Y") * mouseSensitivity;
		//Limit rotation to the boundaries
		verticalRotation = Mathf.Clamp (verticalRotation, -tiltLimit, tiltLimit);
		//Apply rotation to camera
		Camera.main.transform.localRotation = Quaternion.Euler (verticalRotation, 0, 0);

		//////////Spatial movement//////////
		//Movement input
		float forwardSpeed = Input.GetAxis ("Vertical") * moveSpeed;
		float sideSpeed = Input.GetAxis ("Horizontal") * moveSpeed;

		//Gravity
		verticalVelocity += Physics.gravity.y * Time.deltaTime;

		//Jump input
		if (characterController.isGrounded && Input.GetButtonDown("Jump")) {
			verticalVelocity = jumpSpeed;
		}

		//Collect the movement on all axis
		Vector3 speed = new Vector3 (sideSpeed, verticalVelocity, forwardSpeed);
		//Movement is in axis of the rotation ('w' is always forward, 's' is always backward)
		speed = transform.rotation * speed;
		//Apply movement to the characterController and make it move at the same speed regardless of the frame per second
		characterController.Move (speed * Time.deltaTime);

		if (Input.GetKey ("escape")) {
			Application.Quit ();
		}
		
	}

	void OnControllerColliderHit(ControllerColliderHit touch){

		if (touch.gameObject.tag == "waterDeath" && !touching) {
			touching = true;
			Application.LoadLevel ("angryFPS01");
		}

	}
	
}
