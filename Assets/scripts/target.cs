﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class target : MonoBehaviour {

	public Text scoreText;
	public static int score = 0;

	// Use this for initialization
	void Start () {

		Physics.IgnoreLayerCollision (8, 9, true);
		scoreText.text = "Score: " + score;

	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter (Collision collision){

		if (collision.gameObject.tag == "floor" || collision.gameObject.tag == "waterDeath") {

			targetDing.targetTrigger = true;

			score += 1;
			scoreText.text = "Score: " + score;

			Destroy (gameObject);

			shooting.projectileLeft += 5;
		}
	}

	void OnLevelWasLoaded(){
		score = 0;
	}
}
