﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class shooting : MonoBehaviour {

	public Rigidbody projectile;	
	private float speed = 0;
	private float maxSpeed = 22.0F;
	private float speedIncrement;
	private bool reverseSpeed = false;

	public Text projectileText;
	public static int projectileLeft = 10;

	public Slider gunPower;
	private float sliderValue = 0.0F;

	// Use this for initialization
	void Start () {

		speedIncrement = maxSpeed / 100;
		projectileText.text = "Bullets left: " + projectileLeft;

	}
	
	// Update is called once per frame
	void Update () {
	
		projectileText.text = "Bullets left: " + projectileLeft;

		if (projectileLeft != 0) {

			if (Input.GetButton ("Fire1") && speed <= maxSpeed && !reverseSpeed){
			
				speed += speedIncrement;

				sliderValue += 0.01F;
				gunPower.value = sliderValue;			
			}

			if (Input.GetButton ("Fire1") && speed >= 0 && reverseSpeed){

				speed -= speedIncrement;
				
				sliderValue -= 0.01F;
				gunPower.value = sliderValue;
			}

			if (Input.GetButtonUp ("Fire1")) {

				projectileLeft -= 1;
				projectileText.text = "Bullets left: " + projectileLeft;

				sliderValue = 0.0F;
				gunPower.value = sliderValue;
			
				Rigidbody instantiatedProjectile = Instantiate (projectile, transform.position, transform.rotation) as Rigidbody;			
				instantiatedProjectile.velocity = transform.TransformDirection (new Vector3 (0, speed, 0));
			
				speed = 0;
				GetComponent<AudioSource>().Play();
			}

			if (speed >= maxSpeed){
				reverseSpeed = true;
			}

			if (speed <= 0){
				reverseSpeed = false;
			}
		}

	}

	void OnLevelWasLoaded(){
		projectileLeft = 10;
	}
}
